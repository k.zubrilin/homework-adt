sealed trait Tree

case class Node(value: Int, left: Tree, right: Tree) extends Tree

case object RedLeaf extends Tree
case object YellowLeaf extends Tree
case object GreenLeaf extends Tree

object Tree {
  def countYellowAndRedValues(tree: Tree): Int = tree match {
    case Node(value, RedLeaf | YellowLeaf, right) => value + countYellowAndRedValues(right)
    case Node(value, left, RedLeaf | YellowLeaf) => value + countYellowAndRedValues(left)
    case Node(_, left, right) => countYellowAndRedValues(left) + countYellowAndRedValues(right)
    case RedLeaf | YellowLeaf | GreenLeaf => 0
  }

  def maxValue(tree: Tree): Option[Int] = tree match {
    case Node(value, left, right) => Some(List(Some(value), maxValue(left), maxValue(right)).flatten.max)
    case RedLeaf | YellowLeaf | GreenLeaf => None
  }
}

object ShowTree extends App {
  import Tree._

  val testTree = Node(4,
    Node(6, GreenLeaf, GreenLeaf),
    Node(2,
      Node(11, RedLeaf, YellowLeaf),
      Node(3, YellowLeaf, GreenLeaf)))

  println(countYellowAndRedValues(testTree))
  println(maxValue(testTree))
}
