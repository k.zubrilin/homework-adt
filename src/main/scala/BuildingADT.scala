import scala.annotation.tailrec

case class Building(address: String, firstFloor: LivingFloor)

sealed trait Floor
case class LivingFloor(firstLodger: Lodger, secondLodger: Lodger, nextFloor: Floor) extends Floor
case object Attic extends Floor

case class Lodger(sex: Sex, age: Int)

sealed trait Sex
case object Male extends Sex
case object Female extends Sex

object Building {
  def protoFold(building: Building, acc0: Int)(f: (Int, LivingFloor) => Int): Int = {
    @tailrec
    def innerFold(floor: Floor, acc: Int): Int = floor match {
      case livingFloor: LivingFloor => innerFold(livingFloor.nextFloor, f(acc, livingFloor))
      case Attic => acc
    }

    innerFold(building.firstFloor, acc0)
  }

  def countOldManFloors(building: Building, olderThen: Int): Int = {
    def countOnFloor(oldMenBefore: Int, floor: LivingFloor): Int = {
      val LivingFloor(firstLodger, secondLodger, _) = floor
      (firstLodger, secondLodger) match {
        case (Lodger(Male, age), _) if age > olderThen => oldMenBefore + 1
        case (_, Lodger(Male, age)) if age > olderThen => oldMenBefore + 1
        case _ => oldMenBefore
      }
    }

    protoFold(building, 0)(countOnFloor)
  }

  def womanMaxAge(building: Building): Int = {
    def maxAge(maxAgeBefore: Int, floor: LivingFloor): Int = {
      val LivingFloor(firstLodger, secondLodger, _) = floor
      (firstLodger, secondLodger) match {
        case (Lodger(Female, firstAge), Lodger(Female, secondAge)) => List(firstAge, secondAge, maxAgeBefore).max
        case (Lodger(Female, age), _) => List(age, maxAgeBefore).max
        case (_, Lodger(Female, age)) => List(age, maxAgeBefore).max
        case _ => maxAgeBefore
      }
    }
    protoFold(building, 0)(maxAge)
  }
}